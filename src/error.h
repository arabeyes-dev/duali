/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali error.h
 *
 * Error handling header file
 *
 * (C) Copyright 2002,2003,2004 Arabeyes, Mohammed Elzubeir
 * 
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#ifndef ERROR_H
#define ERROR_H 1

#include "general.h"

// BEGIN_C_DECLS

extern void duali_warning (const char *message);
extern void duali_error (const char *message);
extern void duali_fatal (const char *message);

// END_C_DECLS

#endif /* !ERROR_H */
