/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali translitrator.h
 *
 * transliterator class definition done by Mohammed Yousif
 *
 * (C) Copyright 2002,2003,2004 Arabeyes, Mohammed Elzubeir
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#ifndef TRANSLITERATOR_H
#define TRANSLITERATOR_H

#include "general.h"

class Transliterator {
public:
  static DChar Transliterate(DChar);
  static DChar DeTransliterate(DChar);
  static DString Transliterate(DString);
  static DString DeTransliterate(DString);
  static DString DeTransliterate(std::string);
  
private:
  static void Init();
  
  static std::map<char, DChar> t2a;
  static std::map<DChar, char> a2t;

  static bool initialized;
};

#endif
