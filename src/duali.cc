/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali main file
 *
 * (C) Copyright 2002,2003,2004 Arabeyes, Mohammed Elzubeir
 * 
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#include "general.h"

#define DBPREFIX "prefix.db"
#define DBSTEM "stem.db"
#define DBSUFFIX "suffix.db"

int verbose = 0;
char *program_name;

int
usage (FILE * out, int exit_code)
{
  fprintf(out, "(C) Copyright 2003,2004 Arabeyes, Mohammed Elzubeir\n"); 
  fprintf (out, "Usage: %s -c [ inputfile ... ] OPTIONS\n", program_name);
  fprintf (out,
	   "  -h  --help              display this message.\n"
	   "  -V  --version           display version number.\n"
	   "  -c  --check= filename   file to spellcheck.\n"
	   "  -C  --charset= codeset  character encoding (utf-8 or cp1256).\n"
	   "  -n  --normalize         toggle normalization off (default on).\n"
	   "  -p  --path              path to dictionary.\n"
	   "  -v  --verbose           verbose out (for debugging).\n");
  fprintf(out,   "\nThis program is written under the BSD License.\n");
  exit (exit_code);
}

int
main (int argc, char *argv[])
{
  int n_option;
  int ret;
  DB *dbprefix;
  //  DFile mfile;

  const char *output_filename = NULL;
  program_name = argv[0];
  
  const char *const s_options = "hvc:C:np:V";
  const struct option l_options[] = {
    {"help", 0, NULL, 'h'},
    {"version", 0, NULL, 'V'},
    {"check", 1, NULL, 'c'},
    {"charset", 1, NULL, 'C'},
    {"normalize", 0, NULL, 'n'},
    {"path", 1, NULL, 'p'},
    {"verbose", 0, NULL, 'v'},
    {NULL, 0, NULL, 0}
  };
  
  setlocale(LC_CTYPE, "");
  
  if (argc > 1)
    {
      do
	{
	  n_option =
	    getopt_long (argc, argv, s_options, l_options,
			 NULL);
	  switch (n_option)
	    {
	    case 'h':
	      usage (stdout, EXIT_SUCCESS);
	    case 'o':
	      output_filename = optarg;
	      break;
	    case 'c':
	      // mfile.setFilename (optarg);
	      break;
	    case 'v':
	      verbose = 1;
	      break;
	    case 'V':
	      printf ("%s\n", program_version ());
	      exit (EXIT_SUCCESS);
	    case '?':
	      usage (stderr, EXIT_FAILURE);
	    case -1:
	      break;
	    default:
	      exit (EXIT_SUCCESS);
	    }
	}
      while (n_option != -1);
    }
  else
    {
      duali_error
	("Not enough arguments! Please use -h option for usage.");
      exit (EXIT_FAILURE);
    }
  
  if (verbose)
    printf ("[VERBOSE] getting file info\n");

  if (( ret = db_create(&dbprefix, NULL, 0)) != 0) {
    dbprefix->err(dbprefix, ret, "%s", DBPREFIX);
    exit (EXIT_FAILURE);
  }
  
  if (( ret = dbprefix->open(dbprefix, NULL, DBPREFIX, NULL, DB_HASH, 
			 DB_RDONLY, 0644) != 0)) {
    dbprefix->err(dbprefix, ret, "%s", DBPREFIX);
    exit (EXIT_FAILURE);
  }
    
  return EXIT_SUCCESS;
  
}
