#!/usr/bin/env python
#
#---
# $Id$
#
# ------------
# Description:
# ------------
# This script will generate a dictionary from a list of words for use
# with Duali.
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
#---

from aralex import *
import sys, getopt, os

scriptname = os.path.splitext(os.path.basename(sys.argv[0]))[0]
scriptversion = '$Id$'

def arabic_only(lines):
  "temp function to create the word list from the full_wordlist file"
  for l in lines:
    tmp = l.split(' ')
    tlen = len(tmp)
    
    for i in range(2, tlen-1):
      if (i==tlen-1):
        print tmp[i][:-1]
      else:
        print tmp[i]


def grabargs():
  """
  Grab all argument values and returns a tuple of:
  fname        -- filename
  wantroot     -- if 0 show only roots, 1 show ones that couldn't fit
  alphabetical -- if 0 show in random order, 1 show in alphabetical order
  stemit       -- attempt to strip prefix and suffix from the word first
  """

  fname = ''
  wantroot = 0
  stemit = 0
  alphabetical = 0
  
  if not sys.argv[1:]:
    usage()
    sys.exit(0)
    
  try:
    opts, args = getopt.getopt(sys.argv[1:], "hnrasVf:",
                               ["help", "nonroots", "roots", "alphabetical",
                                "stem", "version", "file="],)
  except getopt.GetoptError:
    usage()
    sys.exit(0)

  for o, val in opts:
    if o in ("-h", "--help"):
      usage()
      sys.exit(0)
    if o in ("-V", "--version"):
      print scriptversion
      sys.exit(0)
    if o in ("-n", "--nonroots"):
      wantroot = 1
    if o in ("-r", "--roots"):
      wantroot = 0
    if o in ("-a", "--alphabetical"):
      alphabetical = 1
    if o in ("-s", "--stem"):
      stemit = 1
    if o in ("-f", "--f"):
      fname = val
    
  return (fname, wantroot, alphabetical, stemit)

def usage():
  "Display usage options"
  print "Usage: %s -f filename [OPTIONS]" % scriptname
  print "\t[-h | --help          ]\t\toutputs this usage message"
  print "\t[-V | --version       ]\t\tprogram version"
  print "\t[-f | --file= filename]\t\tinput file containing word list"
  print "\t[-n | --nonroots      ]\t\tgenerate failed words"
  print "\t[-r | --roots         ]\t\tgenerate root words"
  print "\t[-a | --alphabetical  ]\t\toutput in alphabetical order"
  print "\t[-s | --stem          ]\t\tattempt to strip prefix/suffix first"
  print "\r\nThis program is licensed under the BSD License\n"

def main():
  "Main() function"

  (fname, wantroot, alphabetical, stemit) = grabargs()
  lst = []
  
  if (stemit == 1):
    mylex = aralex()
  else:
    mylex = aralex(1)
    
  lines = open(fname, 'r').readlines()

  for line in lines:
    myroot = mylex.getRoot(unicode(line[:-1], 'utf-8'))
        
  if (wantroot == 0):
    dict = mylex.getDict()
    if (alphabetical == 1):
      for word in dict.keys():
        lst.append((word.encode('utf-8'), dict[word][0], dict[word][1], dict[word][2].encode('utf-8')))
      lst.sort()
      for (word, l, d, o) in lst:
        print "%s [l=%d:d=%d:o=%s]" % (word, l, d, o)
    else:
      for word in dict.keys():
        print "%s [l=%d:d=%d:o=%s]" % (word, dict[word][0],
                                       dict[word][1], dict[word][2])

          
  if (wantroot == 1):
    failures = mylex.getFailures()
    for word in failures.keys():
      print word.encode('utf-8')

  sys.exit(0)

if __name__ == "__main__":
  main()
