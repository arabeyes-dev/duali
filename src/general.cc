/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali general.cc
 *
 * General functions for Duali
 *
 * (C) Copyright 2002,2003,2004 Arabeyes, Mohammed Elzubeir
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#include "general.h"

char *
program_version ()
{
  char *str = new char[30];
  
  sprintf (str, "Duali ver. %s", VERSION);
  return str;
}
