/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali dchar.h
 *
 * dchar class definition done by Mohammed Yousif
 *
 * (C) Copyright 2002,2003, 2004 Arabeyes, Mohammed Elzubeir
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#ifndef DCHAR_H
#define DCHAR_H

#include "general.h"

class DChar {
public:
  DChar();
  DChar(const int) throw(std::domain_error);
  DChar(const gunichar) throw(std::domain_error);
  DChar(const char);
  DChar(const char *) throw(std::domain_error);
  DChar(const std::string) throw(std::domain_error);
  
  DChar lower() const;
  DChar upper() const;
  DChar title() const;
  int digitValue() const throw(std::domain_error);
  int hexDigitValue() const throw(std::domain_error);
  
  bool isDefined() const;
  bool isNull() const;
  bool isPrint() const;
  bool isGraph() const;
  bool isPunct() const;
  bool isSpace() const;
  bool isLower() const;
  bool isUpper() const;
  bool isTitle() const;
  bool isAlphaNumeric() const;
  bool isAsciiLetter() const;
  bool isDigit() const;
  bool isHexDigit() const;
  bool isAlphabetic() const;
  bool isControl() const;
  
  int bytes() const;
  std::string utf8() const;
  
  static DChar fromUtf8(const char *) throw(std::domain_error);
  static DChar fromUtf8(const std::string) throw(std::domain_error);
  
  operator gunichar();
  
  friend class DString;
  friend std::ostream & operator<<( std::ostream &, DChar);
  friend bool operator<(const DChar, const DChar);
  friend bool operator>(const DChar, const DChar);

private:
    gunichar ucs4;
};

bool operator<(const DChar, const DChar);
bool operator>(const DChar, const DChar);

std::ostream & operator<<( std::ostream &, DChar);
std::istream & operator>>( std::istream &, DChar &);

#endif
