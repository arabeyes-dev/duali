/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali -- item ADT
 *
 * (C) Copyright 2002,2003,2004 Arabeyes, Mohammed Elzubeir
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#include "general.h"

/**
 * A generic Item container
 */
template < typename Key >
class Item
{
 private:
  Key keyval;
  gchar* d_word;
  
 public:
  Item ()
    {
      keyval = NULL;
    }
  Item (Key k)
    {
      keyval = k;
    }
  
  Key getKey ()
    {
      return keyval;
    }
  
  int null ()
    {
      return keyval == NULL;
    }
};
