/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali Dictionary class
 *
 * Dict class definition
 *
 * (C) Copyright 2002,2003,2004 Arabeyes, Mohammed Elzubeir
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/


#ifndef DICT_H
#define DICT_H 1

using namespace duali;

#include "general.h"

/**
 * Load and retrieve/add entries to/from dictionary
 *
 * This is where all the dictionary related activities occur. Mainly
 * it should be used to interact with the TST data structure where
 * the dictionary is loaded.
 */
class Dict : public Aword {

  /**
   * Load dictionary from file
   *
   */
  virtual void loadDict();

  /**
   * Find a certain word
   *
   * @param gchar *
   * @return gboolean TRUE if found
   */
  virtual gboolean findWord(const gchar *);

  /**
   * Add new entry
   *
   * @param const gchar *word, int forms, gboolean breakable
   */
  virtual gboolean addEntry(const gchar *, int, gboolean);

 public:
  /**
   * Default constructor
   *
   */
  Dict();

  /**
   * Default destructor
   *
   */
  ~Dict();
};

#endif
