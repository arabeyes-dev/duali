/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali dfile.cc
 *
 * Dfile class implementation
 *
 * (C) Copyright 2002,2003,2004 Arabeyes, Mohammed Elzubeir
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#include "general.h"


void
DFile::setFilename (gchar * fname)
{
  int tmp = strlen (fname);
  filename = new char[tmp];
  filename = fname;
}

gchar *
DFile::getFilename () const 
{
  if (strlen (filename) > 0)
    return filename;
  else
    return NULL;
}

gboolean
DFile::loadFile ()
{
  if (!g_file_get_contents (filename, &txt, &fsize, &error))
    return FALSE;
  return g_utf8_validate (txt, fsize, NULL);
}

inline void
DFile::verify (std::ifstream & in, const gchar * fname = "")
{
  using namespace std;
  if (!in)
    {
      cerr << "Could not open file: " << fname << "\n";
      exit (1);
    }
}

inline void
DFile::verify (std::ofstream & out, const gchar * fname = "")
{
  using namespace std;
  if (!out)
    {
      cerr << "Could not open file: " << fname << "\n";
      exit (1);
    }
}

gchar *
DFile::getContents ()
{
  if (this->txt != NULL)
    return this->txt;
}
