#---
# $Id$
#
# ------------
# Description:
# ------------
#
# The Duali araspell class
#
# (C) Copyright 2003, Arabeyes, Mohammed Elzubeir
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
#  This program is written under the BSD License.
#---

class araspell:
    
    def __init__(self, mydict, mylex, charset='utf-8', verbose=0):
        self.charset = charset
        self.verbose = verbose
        self.mylex = mylex
        self.mydict = mydict
        self.prefixes = mydict.getPrefix()
        self.suffixes = mydict.getSuffix()
        self.stems = mydict.getStem()

    def compatcheck(self):
        "check compatibility of prefix, stem and suffix"
        return

    def spellcheck(self, word):
        "Analyze the word and return true if it's valid false if not"
        segwords = self.mylex.segment(word.decode(self.charset))
        counter = 1
        for mypref, mystem, mysuff in segwords:
            if (self.verbose):
                print """
araspell:spellcheck() - pref: %s\t stem: %s\t suff: %s""" % (
                    mypref.encode(self.charset),
                    mystem.encode(self.charset),
                    mysuff.encode(self.charset))
            if (self.stems.has_key(mystem.encode('utf-8'))
                and self.suffixes.has_key(mysuff.encode('utf-8'))):
                return 1
        return 0
