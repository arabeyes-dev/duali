#!/usr/bin/env python
#
#---
# $Id$
#
# ------------
# Description:
# ------------
# 
# This setup script will create executable distributions for the Windows
# environment.
# 
# You will need py2exe module installed.
# 
# How to run it:
#
# C:\path\to\script> python setup.py py2exe --packages encodings
#
# Note the 'packages' argument, if you don't include it, UTF-8 and CP1256
# encoding related functionalities may not work properly.
#
#
# (C) Copyright 2003, Arabeyes, Mohammed Elzubeir
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
#  This program is written under the BSD License.
#---


from distutils.core import setup
import py2exe

setup(name="dict2csv",
      version="1.0",
      description="Convert Buckwalter dictionary to Excel readable spreadsheet",
      author="Mohammed Elzubeir",
      author_email="elzubeir@arabeyes.org",
      py_modules = ['arabic', 'trans_table'],
      scripts= ['trans2arabic.py'])
      
