/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali dregex.cc
 *
 *
 * (C) Copyright 2002,2003, 2004 Arabeyes, Mohammed Elzubeir
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#include "general.h"

DRegex::DRegex(): valid(false)
{
}

DRegex::DRegex(const char * exp, int flags): valid(false)
{
  compile(exp, flags);
}

DRegex::DRegex(const std::string & exp, int flags): valid(false)
{
  compile(exp, flags);
}

DRegex::~DRegex()
{
  reset();
}

void DRegex::reset()
{
  if (valid) {
    regfree(&re);
    currentMatch = "";
    markers.clear();
    valid = false;
  }
}

void DRegex::compile(const char * exp, int flags) throw(DRegex::regex_error)
{
  if (valid)
    reset();
  std::string expression(exp);
  
  // Translate from PERL syntax into POSIX syntax (only \S is currently supported)
  // TODO: Use DRegex itself here if it proves to be faster :-)
  // TODO: Add support for common PERL syntax escape sequences.
  int offset = -2;
  while((offset = expression.find("\\S", offset + 2)) != std::string::npos)
    expression.replace(offset, 2, "[^[:space:]]");
  
  int r = regcomp(&re, expression.c_str(), flags | REG_EXTENDED);
  if (r != 0) {
    throwException(r);
  }
  valid = true;
}

void DRegex::compile(const std::string & exp, int flags) throw(DRegex::regex_error)
{
  compile(exp.c_str(), flags);
}

int DRegex::match(const std::string & str, std::string::size_type offset, int eflags) throw(std::domain_error)
{
  if (!valid)
    throw std::domain_error("the regular expression has not been compiled yet");
  currentMatch = str;
  markers.clear();
  markers.reserve(re.re_nsub + 1);
  regmatch_t noMarker = {-2, -2};
  markers.insert(markers.begin(), re.re_nsub + 1, noMarker);
  int r = regexec(&re, str.c_str() + offset, re.re_nsub + 1, &markers[0], eflags);
  switch (r) {
  case REG_NOMATCH:
    return 0;
  case 0:
    return markers.size();
   default:
      throwException(r);
  }
}

std::string DRegex::sub(const std::string & str, const std::string & m) throw(std::domain_error)
{
  std::stringstream buff; 
  int offset = 0;
  for (;;) {
    if (match(str, offset) > 0) {
      buff << str.substr(offset, markers[0].rm_so) << m;
      offset += markers[0].rm_eo;
    } else {
      buff << str.substr(offset);
      break;
    }
  }
  return buff.str();
}

void DRegex::throwException(int errcode) throw(DRegex::regex_error)
{
  int len = regerror(errcode, &re, 0, 0);
  char * buf = new char[len];
  regerror(errcode, &re, buf, len);
  std::string msg(buf);
  delete [] buf;
  throw DRegex::regex_error(msg);
}

std::string DRegex::operator[](int id) const throw(std::out_of_range)
{
  if (!valid)
    throw std::domain_error("the regular expression has not been compiled yet");
  if (id >= markers.size())
    throw std::out_of_range("the provided match index is out of range or there are no matches");
  return std::string(currentMatch.begin() + markers[id].rm_so, currentMatch.begin() + markers[id].rm_eo);
}

bool operator==(const regmatch_t & lhs, const regmatch_t & rhs)
{
  return (lhs.rm_so == rhs.rm_so) && (lhs.rm_eo == rhs.rm_eo);
}
