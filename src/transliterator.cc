/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali transliterator.h
 *
 * transliterator class implementation done by Mohammed Yousif
 *
 * (C) Copyright 2002,2003,2004 Arabeyes, Mohammed Elzubeir
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#include "general.h"

std::map<char, DChar> Transliterator::t2a;
std::map<DChar, char> Transliterator::a2t;
bool Transliterator::initialized;

void Transliterator::Init()
{
  t2a['A'] = ALEF;
  t2a['b'] = BEH;
  t2a['t'] = TEH;
  t2a['p'] = TEH_MARBUTA;
  t2a['v'] = THEH;
  t2a['j'] = JEEM;
  t2a['H'] = HAH;
  t2a['x'] = KHAH;
  t2a['d'] = DAL;
  t2a['*'] = THAL;
  t2a['r'] = REH;
  t2a['z'] = ZAIN;
  t2a['s'] = SEEN;
  t2a['$'] = SHEEN;
  t2a['S'] = SAD;
  t2a['D'] = DAD;
  t2a['T'] = TAH;
  t2a['Z'] = ZAH;
  t2a['E'] = AIN;
  t2a['g'] = GHAIN;
  t2a['f'] = FEH;
  t2a['q'] = QAF;
  t2a['k'] = KAF;
  t2a['l'] = LAM;
  t2a['m'] = MEEM;
  t2a['n'] = NOON;
  t2a['h'] = HEH;
  t2a['w'] = WAW;
  t2a['y'] = YEH;
  t2a['Y'] = ALEF_MAKSURA;
  t2a['\''] = HAMZA;
  t2a['&'] = WAW_HAMZA;
  t2a['>'] = ALEF_HAMZA_ABOVE;
  t2a['<'] = ALEF_HAMZA_BELOW;
  t2a['|'] = ALEF_MADDA;
  t2a['}'] = YEH_HAMZA;
  t2a['_'] = TATWEEL;
  t2a['a'] = FATHA;
  t2a['F'] = FATHATAN;
  t2a['i'] = KASRA;
  t2a['K'] = KASRATAN;
  t2a['u'] = DAMMA;
  t2a['N'] = DAMMATAN;
  t2a['~'] = SHADDA;
  t2a['o'] = SUKUN;
  t2a['`'] = MINI_ALEF;
  t2a['{'] = ALEF_WASLA;
  
  a2t[ALEF] = 'A';
  a2t[BEH] = 'b';
  a2t[TEH] = 't';
  a2t[TEH_MARBUTA] = 'p';
  a2t[THEH] = 'v';
  a2t[JEEM] = 'j';
  a2t[HAH] = 'H';
  a2t[KHAH] = 'x';
  a2t[DAL] = 'd';
  a2t[THAL] = '*';
  a2t[REH] = 'r';
  a2t[ZAIN] = 'z';
  a2t[SEEN] = 's';
  a2t[SHEEN] = '$';
  a2t[SAD] = 'S';
  a2t[DAD] = 'D';
  a2t[TAH] = 'T';
  a2t[ZAH] = 'Z';
  a2t[AIN] = 'E';
  a2t[GHAIN] = 'g';
  a2t[FEH] = 'f';
  a2t[QAF] = 'q';
  a2t[KAF] = 'k';
  a2t[LAM] = 'l';
  a2t[MEEM] = 'm';
  a2t[NOON] = 'n';
  a2t[HEH] = 'h';
  a2t[WAW] = 'w';
  a2t[YEH] = 'y';
  a2t[ALEF_MAKSURA] = 'Y';
  a2t[HAMZA] = '\'';
  a2t[WAW_HAMZA] = '&';
  a2t[ALEF_HAMZA_ABOVE] = '>';
  a2t[ALEF_HAMZA_BELOW] = '<';
  a2t[ALEF_MADDA] = '|';
  a2t[YEH_HAMZA] = '}';
  a2t[TATWEEL] = '_';
  a2t[FATHA] = 'a';
  a2t[FATHATAN] = 'F';
  a2t[KASRA] = 'i';
  a2t[KASRATAN] = 'K';
  a2t[DAMMA] = 'u';
  a2t[DAMMATAN] = 'N';
  a2t[SHADDA] = '~';
  a2t[SUKUN] = 'o';
  a2t[MINI_ALEF] = '`';
  a2t[ALEF_WASLA] = '{';
  a2t[COMMA] = ';';

  initialized = true;
}

DChar Transliterator::Transliterate(DChar c)
{
  if (!initialized)
    Init();
  std::map<DChar, char>::iterator it = a2t.find(c);
  if(it != a2t.end())
    return it->second;
  else
    return c;
}

DChar Transliterator::DeTransliterate(DChar c)
{
  if (!initialized)
    Init();

  std::map<char, DChar>::iterator it = t2a.find(c);
  if(it != t2a.end())
    return it->second;
  else
    return c;
}

DString Transliterator::Transliterate(DString str)
{
  if (!initialized)
    Init();
  DString transliterated("");
  for (int i=0; i<str.length(); i++) {
    std::map<DChar, char>::iterator it = a2t.find(str[i]);
    if(it != a2t.end())
      transliterated += it->second;
    else
      transliterated += str[i];
  }
  return transliterated;
}

DString Transliterator::DeTransliterate(DString str)
{
  return DeTransliterate(str.utf8());
}

DString Transliterator::DeTransliterate(std::string str)
{
  if (!initialized)
    Init();
  DString detransliterated("");
  for (int i=0; i<str.length(); i++) {
    std::map<char, DChar>::iterator it = t2a.find(str[i]);
    if(it != t2a.end())
      detransliterated += it->second;
    else
      detransliterated += str[i];
  }
  return detransliterated;
}
