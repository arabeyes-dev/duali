/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali dstring.h
 *
 * dstring class definition done by Mohammed Yousif
 *
 * (C) Copyright 2002,2003,2004 Arabeyes, Mohammed Elzubeir
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#ifndef DSTRING_H
#define DSTRING_H

#include "general.h"

class DString {
public:
  DString();
  DString(const char *);
  DString(const std::string);
  DString(const std::list<DChar>);
  
  bool isEmpty() const;
  
  int length() const;
  int bytes() const;
  DString lower() const;
  DString upper() const;
  DString caseFold() const;
  DString reverse() const;
  
  DChar at(int) const throw(std::out_of_range);
  void setAt(int, DChar) throw(std::out_of_range);
  
  int compareTo(const DString &) const;
  DString substr(int, int = std::string::npos) const throw(std::out_of_range);

  const std::string utf8() const;
  const char * c_str() const;
  const char * data() const;

  
  operator std::string() const;
  operator const char *() const;
  
  DChar operator[](int) const throw(std::out_of_range);
  
  // Assignment Overloads
  DString & operator=(const std::string &);
  DString & operator=(const char *);
  DString & operator=(const DChar);
  DString & operator=(const char);
  DString & operator=(const gunichar);
  
  // Appending Overloads
  DString & operator+=(const DString &);
  DString & operator+=(const std::string &);
  DString & operator+=(const char *);
  DString & operator+=(const DChar);
  DString & operator+=(const char);
  DString & operator+=(const gunichar);
  
  friend DString operator+(const DString &, const DString &);
  friend DString operator+(const DString &, const char *);
  friend DString operator+(const char *, const DString &);
  friend DString operator+(const DString &, const DChar);
  friend DString operator+(const DChar, const DString &);
  friend DString operator+(const DString &, const char);
  friend DString operator+(const char, const DString &);
  friend DString operator+(const DString &, const gunichar);
  friend DString operator+(const gunichar, const DString &);
  
private:
  std::string raw;
};

bool operator==(const DString &, const DString &);
bool operator==(const DString &, const char *);
bool operator==(const char *, const DString &);

bool operator!=(const DString &, const DString &);
bool operator!=(const DString &, const char *);
bool operator!=(const char *, const DString &);

bool operator<(const DString &, const DString &);
bool operator<(const DString &, const char *);
bool operator<(const char *, const DString &);

bool operator<=(const DString &, const DString &);
bool operator<=(const DString &, const char *);
bool operator<=(const char *, const DString &);

bool operator>(const DString &, const DString &);
bool operator>(const DString &, const char *);
bool operator>(const char *, const DString &);

bool operator>=(const DString &, const DString &);
bool operator>=(const DString &, const char *);
bool operator>=(const char *, const DString &);

DString operator+(const DString &, const DString &);
DString operator+(const DString &, const char *);
DString operator+(const char *, const DString &);
DString operator+(const DString &, const DChar);
DString operator+(const DChar, const DString &);
DString operator+(const DString &, const char);
DString operator+(const char, const DString &);
DString operator+(const DString &, const gunichar);
DString operator+(const gunichar, const DString &);
DString operator+(const DChar, const DChar);
DString operator+(const char *, const DChar);
DString operator+(const DChar, const char *);

#endif
