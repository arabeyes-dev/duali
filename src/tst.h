/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali -- TST ADT Class definition
 *
 * (C) Copyright 2002,2003,2004 Arabeyes, Mohammed Elzubeir
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#include "general.h"

/**
 * Ternary Search Trie ADT class implementation
 *
 * This is where we make use of some advanced data structures to store
 * our dictionary words (and other related information).
 */

template<typename Item, typename Key> 
class TST
{
 private: 
  /**
   * Node structure for the TST
   */
  struct node
  {
    Item item;
    int d;
    node *l, *m, *r;
    node (Item x, int k)
    {
      item = x;
      d = k;
      l = m = r = 0;
    }
    node (node * h, int k)
    {
      d = k;
      m = h;
      l = r = 0;
    }
    int internal ()
    {
      return d != NULLdigit;
    }
  };
  
  typedef node *link;
  link heads[28];
  Item nullItem;
  static int NULLdigit;
  /**
   *  Use bits of the key to control branching
   *  during a search
   *  
   *  @param link, Key, int
   */
  Item searchP (link, Key, int);
  
  
  /**
   * new existence trie
   */
  link newext (Item);
  
  /**
   * split()
   */
  link split (link, link, int);
  
  /**
   *  insertion of a new node
   */
  void insertP (link &, Item, int);
  
  /**
   * digit()
   * 
   * Return the ith digit from a key
   */
  int digit (int, int);
  //  int digit(float, int);
  int digit (char *, int);
  
 public:
  
  /**
   * Constructor
   *
   */
  // default
  TST ();
  TST (int);
  
  void insert (Item);
  Item search (Key);
  void remove (Item);
  Item select (int);
  // void show(ostream&);
};
