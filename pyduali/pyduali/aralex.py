#---
# $Id$
#
# ------------
# Description:
# ------------
# The aralex class should perform analysis on a given string (an Arabic word)
# and decide what its root is.
#
# When getRoot() is called, it stores the resulting root word in 'dict'
# else it will store it in 'nonroots'. Which are the words it didn't know
# how to deal with.
#
# (C) Copyright 2002-03, Arabeyes, Mohammed Elzubeir
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
#  This program is written under the BSD License.
#---

import re, string
from arabic import *

class aralex:

  def __init__(self, nostem = None, verbose = 0):
    self.verbose = verbose
    self.dict = {}
    self.nonroots = {}
    self.nostem = nostem
        
  def getLength(self, ustr):
    "Return the length of an Arabic string"
    return len(ustr)


  def stripextras(self, s):
    return re.sub(r'[%s%s%s%s\\]' % (string.punctuation, string.digits,
                                     string.ascii_letters, string.whitespace),
                  ' ', s)

  def stripDiacritics(self, ustr):
    "Strip diacritics from word and returns clean unicode string"
    return re.sub(ur'[%s%s%s%s%s%s%s%s%s]' % (FATHATAN, DAMMATAN, TATWEEL,
                                            KASRATAN, FATHA, DAMMA, KASRA,
                                            SUKUN, SHADDA),
                  '', ustr)

  def stripPunctuations(self, ustr):
    return re.sub(ur'[%s%s%s%s%s%s%s%s]' % (COMMA, SEMICOLON, QUESTION, ZERO,
                                            PERCENT, THOUSANDS, STAR,
                                            FULL_STOP), '', ustr)
  

  def segment(self, ustr):
    """
    Segment word to prefix, stem, suffix - return list of possible
    combinations
    """
    seg_words = []	
    pref_len = suf_len = 0
    str_len = len(ustr)
    while (pref_len <= 4):
      prefix = ustr[0:pref_len]
      stem_len = str_len - pref_len
      suf_len = 0
      while (stem_len >= 1 and suf_len <= 6):
        stem = ustr[pref_len:stem_len+pref_len]
        suffix = ustr[pref_len+stem_len:]
        seg_words.append((prefix, stem, suffix))
        stem_len -= 1
        suf_len += 1
      pref_len +=1
    return seg_words

  def normalize(self, ustr):
    "Normalize word"
    ustr1 = re.sub(ur'[%s%s%s]' % ( ALEF_MADDA, ALEF_HAMZA_ABOVE,
                                    ALEF_HAMZA_BELOW), ALEF, ustr)
    ustr2 = re.sub(ur'%s%s' % (YEH, HAMZA), YEH_HAMZA, ustr1)
    ustr3 = re.sub(ur'%s' % ALEF_MAKSURA, YEH, ustr2)
    return re.sub(ur'%s' % TEH_MARBUTA, HEH, ustr3)
	
  def getRoot4(self, ustr):
    "Return the root of a 4-letter word"
    m1 = re.match(ur'^(\S\S)([%s%s%s])(\S)$' % (ALEF, YEH, WAW), ustr)
    m2 = re.match(ur'^(\S)([%s%s%s%s%s])(\S\S)$' \
                  % (ALEF, WAW, TAH, DAL, YEH), ustr)
    if (m1):
      myroot = ustr[:2] + ustr[3]
      self.dict[myroot] = (4, 1, ustr)
      return myroot
    elif (m2):
      myroot =  ustr[0] + ustr[2:]
      self.dict[myroot] = (4, 2, ustr)
      return myroot
    else:
      self.nonroots[ustr] = 4
      return None

  def getRoot5(self, ustr):
    "Return the root of a 5-letter word"
    m1 = re.match(ur'^(\S\S)%s%s(\S)$' % (ALEF, ALEF), ustr)
    m2 = re.match(ur'^(\S)([%s,%s])(\S)%s(\S)$' % (TEH, YEH, ALEF),ustr)
    m3 = re.match(ur'^(\S)%s%s(\S\S)$' % (WAW, ALEF), ustr)
    m4 = re.match(ur'^(\S)%s(\S)([%s%s])(\S)$' % (ALEF, YEH, WAW), ustr)
    m5 = re.match(ur'^(\S\S\S)([%s%s%s])(\S)$' % (ALEF, YEH, WAW), ustr)
    m6 = re.match(ur'^(\S\S)([%s%s])(\S\S)$' % (ALEF, YEH), ustr)
    if (m1):
      myroot = ustr[:2] + ustr[4:]
      self.dict[myroot] = (5, 1, ustr)
      return myroot
    elif (m2):
      myroot = ustr[0] + ustr[2] + ustr[4]
      self.dict[myroot] = (5, 2, ustr)
      return myroot
    elif (m3):
      myroot = ustr[0] + ustr[3:]
      self.dict[myroot] = (5, 3, ustr)
      return myroot
    elif (m4):
      myroot = ustr[0] + ustr[2] + ustr[4]
      self.dict[myroot] = (5, 4, ustr)
      return myroot
    elif (m5):
      myroot = ustr[:3] + ustr[4]
      self.dict[myroot] = (5, 5, ustr)
      return myroot
    elif (m6):
      myroot = ustr[:2] + ustr[3:]
      self.dict[myroot] = (5, 6, ustr)
      return myroot
    else:
      self.nonroots[ustr] = 5
      return None

  def getRoot6(self, ustr):
    "Return the root of a 6-letter word"
    m1 = re.match(ur'^(\S)%s%s(\S)%s(\S)$' % (WAW, ALEF, YEH), ustr)
    m2 = re.match(ur'^(\S)(\S)%s(\S)%s(\S)$' % (ALEF, YEH), ustr)
    if (m1):
      myroot =  ustr[0] + ustr[3] + ustr[5]
      self.dict[myroot] = (6, 1, ustr)
      return myroot
    elif (m2):
      myroot = ustr[:2] + ustr[3] + ustr[5:]
      self.dict[myroot] = (6, 2, ustr)
      return myroot
    else:
      self.nonroots[ustr] = 6
      return None

  def getStem(self, ustr):
    "Return the stem, stripping prefix and suffix"
    m = re.search(ur'^([%s]%s|[%s]%s|[%s]%s|[%s]%s|[%s]%s|[%s]%s|)(.+?)(%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s[%s]|%s|%s|%s|%s|%s[%s]|[%s]|)$' % (
      WAW + FEH + BEH,
      ALEF + LAM,
      BEH + YEH + LAM + MEEM + TEH + WAW + SEEN + NOON,
      TEH,
      BEH + LAM + WAW + KAF + FEH,
      MEEM,
      ALEF + LAM,
      LAM,
      WAW + LAM + SEEN + FEH,
      YEH,
      WAW + FEH + LAM + BEH,
      ALEF,
      ALEF + TEH,
      WAW + ALEF,
      TEH + ALEF,
      WAW + NOON,
      WAW + HEH,
      ALEF + NOON,
      TEH + YEH,
      TEH + HEH,
      TEH + MEEM,
      KAF + MEEM,
      HEH,
      NOON + MEEM,
      HEH + ALEF,
      YEH + TEH_MARBUTA,
      TEH + KAF,
      NOON + ALEF,
      YEH,
      NOON + HEH,
      TEH_MARBUTA + HEH + YEH + ALEF),
                ustr)
  
    if (m):
      return m.group(2)
    else:
      return ustr

  def getRoot(self, ustr):
    "Return the root of an Arabic word"
    if (self.nostem):
      ustr = self.stripDiacritics(ustr)
    else:
      ustr = self.stripDiacritics(self.getStem(ustr))
    l = self.getLength(ustr)
    
    if (l <=3 and l > 1):
      self.dict[ustr] = (l, 0, ustr)
      return ustr
    elif (l == 4):
      return self.getRoot4(ustr)
    elif (l == 5):
      return self.getRoot5(ustr)
    elif (l == 6):
      return self.getRoot6(ustr)
    else:
      self.nonroots[ustr] = 0
      return None

  def getDict(self):
    return self.dict

  def getFailures(self):
    return self.nonroots
