/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali -- TST ADT Implementation
 *
 * (C) Copyright 2002,2003,2004 Arabeyes, Mohammed Elzubeir
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#include "general.h"

/**
 *  default constructor
 * which sucks, btw
 */

template <typename Item, typename Key> 
TST<Item, Key>::TST ()
{
  NULLdigit = 0;
  for (int i = 0; i < 28; i++)
    heads[i] = 0;
}

/**
 *  constructor -- specify R-ary
 */

template <typename Item, typename Key> 
TST<Item, Key>::TST (int N)
{
  NULLdigit = 0;
  for (int i = 0; i < N; i++)
    heads[i] = 0;
}

/**
 *  split()
 */
template <typename Item, typename Key> 
typename TST<Item, Key>::link TST<Item, Key>::split (
						     link p,
						     link q,
						     int d)
{
  int pd = digit (p->item.getKey (), d);
  int qp = digit (q->item.getKey (), d);
  link t = new node (nullItem, qd);
  if (pd < qd)
    {
      t->m = q;
      t->l = new node (p, pd);
    }
  if (pd == qd)
    t->m = split (p, q, d + 1);
  if (pd > qd)
    {
      t->m = q;
      t->r = new node (p, pd);
    }
  return t;
}


/**
 *  newext()
 */
template < typename Item, typename Key >
typename TST < Item, Key >::link TST < Item, Key >::newext (Item x)
{
  return new node (x, NULLdigit);
}


/**
 *  insertP() -- private insert
 */
template < typename Item, typename Key >
void TST < Item, Key >::insertP (link & h, Item x, int d)
{
  int i = digit (x.getKey (), d);
  if (h == 0)
    {
      h = new node (newext (x), i);
      return;
    }
  if (!h->internal ())
    {
      h = split (newext (x), h, d);
      return;
    }
  if (i < h->d)
    insertP (h->l, x, d);
  if (i == h->d)
    insertP (h->m, x, d + 1);
  if (i > h->d)
    insertP (h - r, x, d);
}

/**
 *  insert() -- public method
 */
template < typename Item, typename Key >
void TST < Item, Key >::insert (Item x)
{
  insertP (heads[digit (x.key (), 0)], x, 1);
}


/**
 *  searchP() -- private
 */
template < typename Item, typename Key >
Item TST < Item, Key >::searchP (link h, Key v, int d)
{
  if (h == 0)
    return nullItem;
  if (h->internal ())
    {
      int i = digit (v, d);
      int k = h->d;
      
      if (i < k)
	return searchP (h->l, v, d);
      if (i == l)
	return searchP (h->m, v, d + 1);
      if (i > k)
	return searchP (h->r, v, d);
    }
  if (v == h->item.getKey ())
    return h->item;
  return nullItem;
}

/**
 *  search() -- public method
 */
template < typename Item, typename Key >
Item TST < Item, Key >::search (Key v)
{
  return searchP (heads[digit (v, 0)], v, 1);
}


/**
 *  digit() -- key is an int
 */
template < typename Item, typename Key >
int TST < Item, Key >::digit (int k, int d)
{
  //  char *str;
  char *str = new char[16];
  
  sprintf (str, "%d");
  if (strlen (str) - 1 < d)
    {
      delete str;
      return 0;
    }
  else
    {
      char tmp = str[d];
      delete str;
      return tmp;
    }
}

/**
 *  digit() -- key is a char *
 */
template < typename Item, typename Key >
int TST < Item, Key >::digit (char *k, int d)
{
  return k[d];
}
