/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali error.cc
 *
 * Error handling header file
 *
 * (C) Copyright 2002,2003,2004 Arabeyes, Mohammed Elzubeir
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#include "general.h"

static void error (int exit_status, const char *mode, const char *message);

static void
error (int exit_status, const char *mode, const char *message)
{
  fprintf (stderr, "%s: %s: %s.\n", program_name, mode, message);
  
  if (exit_status >= 0)
    exit (exit_status);
}

void
duali_warning (const char *message)
{
  error (-1, "warning", message);
}

void
duali_error (const char *message)
{
  error (-1, "ERROR", message);
}

void
duali_fatal (const char *message)
{
  error (EXIT_FAILURE, "FATAL", message);
}
