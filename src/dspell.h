/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali dspell.h
 *
 * dspell class definition
 *
 * (C) Copyright 2002,2003,2004 Arabeyes, Mohammed Elzubeir
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#ifndef DSPELL_H
#define DSPELL_H 1


#include "general.h"

class dspell : private lexicon {
 public:
  dspell(DB, lexicon, char *, int);

};

#endif
