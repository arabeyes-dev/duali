/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali dregex.h
 *
 *
 * (C) Copyright 2002,2003, 2004 Arabeyes, Mohammed Elzubeir
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#ifndef DREGEX_H
#define DREGEX_H

#include "general.h"

class DRegex {
public:
  
  /**
   * regex_error class
   */
  class regex_error: public std::runtime_error {
  public:
    /**
     * regex_error(std::string) constructor
     *
     * @param std::string
     */
    regex_error(std::string msg): runtime_error(msg.c_str()) {}
  };
  
  DRegex();
  DRegex(const char *, int = 0);
  DRegex(const std::string &, int = 0);
  ~DRegex();
  
  void reset();
  
  void compile(const char *, int = 0) throw(regex_error);
  void compile(const std::string &, int = 0) throw(regex_error);
  
  int match(const std::string &, std::string::size_type = 0, int = 0)  throw(std::domain_error);
  std::string sub(const std::string &, const std::string &) throw(std::domain_error);
  
  void throwException(int) throw(regex_error);
  
  std::string operator[](int) const throw(std::out_of_range);
  
private:
    regex_t re;
    bool valid;
    std::string currentMatch;
    std::vector<regmatch_t> markers;
};

#endif // DREGEX_H
