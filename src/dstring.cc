/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali dstring.cc
 *
 * dstring class implementation done by Mohammed Yousif
 *
 * (C) Copyright 2002,2003,2004 Arabeyes, Mohammed Elzubeir
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#include "general.h"
  
DString::DString(): raw("")
{
}

DString::DString(const char * str): raw(str)
{
}

DString::DString(const std::string str): raw(str)
{
}

DString::DString(const std::list<DChar> arr): raw("")
{
  for (std::list<DChar>::const_iterator i = arr.begin();
       i != arr.end(); i++) {
    raw += i->utf8();
  }
}

bool DString::isEmpty() const
{
  return raw.length() == 0;
}

int DString::length() const
{
  return g_utf8_strlen(raw.c_str(), raw.length());
}

int DString::bytes() const
{
  return raw.length();
}
  
DString DString::lower() const
{
  char * buff = g_utf8_strdown(raw.c_str(), raw.length());
  DString str(buff);
  g_free(buff);
}

DString DString::upper() const
{
  char * buff = g_utf8_strup(raw.c_str(), raw.length());
  DString str(buff);
  g_free(buff);
}

DString DString::caseFold() const
{
  char * buff = g_utf8_casefold(raw.c_str(), raw.length());
  DString str(buff);
  g_free(buff);
}

DString DString::reverse() const
{
  return DString(g_utf8_strreverse(raw.c_str(), -1));
}

DChar DString::at(int index) const throw(std::out_of_range)
{
  if (index >= length() || index < 0)
    throw std::out_of_range("the provided character index is out of range");
  gchar * p;
  p = g_utf8_offset_to_pointer(raw.c_str(), index);
  return DChar(p);
}

void DString::setAt(int index, DChar c) throw(std::out_of_range)
{
  if (index >= length() || index < 0)
    throw std::out_of_range("the provided character index is out of range");
  
  // First, get the character as utf-8 string
  char buff[7];
  int len = g_unichar_to_utf8(c.ucs4, buff);
  buff[len] = '\0';

  // Second, get a pointer to the first byte of the character to be changed
  gchar * p = g_utf8_offset_to_pointer(raw.c_str(), index);
  
  // Third, calculate the index of the first byte of that character by using
  // the obtained pointer
  int startingByte = p - raw.c_str();
  
  // Forth, calculate how many bytes are used for that character
  if (index == length() - 1)
    // if it's the last character, the length is the remaining bytes after p
    len = strlen(p);
  else {
    // if it's not the last character, get a pointer to the first byte
    // of the next character
    gchar * nextP = g_utf8_offset_to_pointer(raw.c_str(), index + 1);
    
    // calculate the index of the first byte of that character by using
    // the obtained pointer
    int nextStartingByte = nextP - raw.c_str();
    
    // Now, calculate the length of the character to be changed
    // length = address(first byte of the next character) -
    //          address(first byte of the character to be changed)
    len = nextStartingByte - startingByte;
  }
  
  // Lastly, replace the sequence of bytes that forms the character to
  // be changed with those of the new character
  raw.replace(startingByte, len, std::string(buff));
}

int DString::compareTo(const DString & rhs) const
{
  return g_utf8_collate(raw.c_str(), rhs.raw.c_str());
}

DString DString::substr(int index, int len) const throw(std::out_of_range)
{
  if (len == 0)
    return "";
  int start = index;
  int end = length() - 1;
  if (len != std::string::npos)
    end = index + len - 1;
  if (start >= length() || start < 0 ||
      end >= length()   || end < 0)
    throw std::out_of_range("the provided character index/substring length is out of range");
  
  gchar * pStart = g_utf8_offset_to_pointer(raw.c_str(), start);
  int rawStartChar = pStart - raw.c_str();
  gchar * pEnd = g_utf8_offset_to_pointer(raw.c_str(), end);
  int rawEndChar = pEnd - raw.c_str();

  int rawLen = rawEndChar - rawStartChar + DChar(pEnd).bytes();
  return DString(raw.substr(rawStartChar, rawLen));
}

const std::string DString::utf8() const
{
  return raw;
}

const char * DString::c_str() const
{
  return raw.c_str();
}

const char * DString::data() const
{
  return raw.data();
}

DString::operator std::string() const
{
  return raw;
}

DString::operator const char *() const
{
  return raw.c_str();
}

DChar DString::operator[](int index) const throw(std::out_of_range)
{
  return at(index);
}

DString & DString::operator=(const std::string & rhs)
{
  raw = rhs;
  return *this;
}

DString & DString::operator=(const char * rhs)
{
  raw = std::string(rhs);
  return *this;
}

DString & DString::operator=(const DChar rhs)
{
  raw = rhs.utf8();
  return *this;
}

DString & DString::operator=(const char rhs)
{
  raw = rhs;
  return *this;
}

DString & DString::operator=(const gunichar rhs)
{
  raw = DChar(rhs).utf8();
  return *this;
}

  
DString & DString::operator+=(const DString & rhs)
{
  return *this = *this + rhs;
}

DString & DString::operator+=(const std::string & rhs)
{
  raw += rhs;
  return *this;
}

DString & DString::operator+=(const char * rhs)
{
  raw += std::string(rhs);
  return *this;
}

DString & DString::operator+=(const DChar rhs)
{
  raw += rhs.utf8();
  return *this;
}

DString & DString::operator+=(const char rhs)
{
  raw.append(&rhs, 1);
  return *this;
}

DString & DString::operator+=(const gunichar rhs)
{
  raw += DChar(rhs).utf8();
  return *this;
}

bool operator==(const DString & lhs, const DString & rhs)
{
  return lhs.compareTo(rhs) == 0;
}

bool operator==(const DString & lhs, const char * rhs)
{
  return lhs.compareTo(DString(rhs)) == 0;
}

bool operator==(const char * lhs, const DString & rhs)
{
  return DString(lhs).compareTo(rhs) == 0;
}

bool operator!=(const DString & lhs, const DString & rhs)
{
  return lhs.compareTo(rhs) != 0;
}

bool operator!=(const DString & lhs, const char * rhs)
{
  return lhs.compareTo(DString(rhs)) != 0;
}

bool operator!=(const char * lhs, const DString & rhs)
{
  return DString(lhs).compareTo(rhs) != 0;
}

bool operator<(const DString & lhs, const DString & rhs)
{
  return lhs.compareTo(rhs) == -1;
}

bool operator<(const DString & lhs, const char * rhs)
{
  return lhs.compareTo(DString(rhs)) == -1;
}

bool operator<(const char * lhs, const DString & rhs)
{
  return DString(lhs).compareTo(rhs) == -1;
}

bool operator<=(const DString & lhs, const DString & rhs)
{
  return lhs.compareTo(rhs) <= 0;
}

bool operator<=(const DString & lhs, const char * rhs)
{
  return lhs.compareTo(DString(rhs)) <= 0;
}

bool operator<=(const char * lhs, const DString & rhs)
{
  return DString(lhs).compareTo(rhs) <= 0;
}

bool operator>(const DString & lhs, const DString & rhs)
{
  return lhs.compareTo(rhs) == 1;
}

bool operator>(const DString & lhs, const char * rhs)
{
  return lhs.compareTo(DString(rhs)) == 1;
}

bool operator>(const char * lhs, const DString & rhs)
{
  return DString(lhs).compareTo(rhs) == 1;
}

bool operator>=(const DString & lhs, const DString & rhs)
{
  return lhs.compareTo(rhs) >= 0;
}

bool operator>=(const DString & lhs, const char * rhs)
{
  return lhs.compareTo(DString(rhs)) >= 0;
}

bool operator>=(const char * lhs, const DString & rhs)
{
  return DString(lhs).compareTo(rhs) >= 0;
}

DString operator+(const DString & lhs, const DString & rhs)
{
  return DString(lhs.raw + rhs.raw);
}

DString operator+(const DString & lhs, const char * rhs)
{
  DString str = lhs;
  str.raw.append(rhs);
  return str;
}
DString operator+(const char * lhs, const DString & rhs)
{
  return DString(DString(lhs) + rhs);
}

DString operator+(const DString & lhs, const DChar rhs)
{
  return DString(lhs + DString(rhs.utf8()));
}

DString operator+(const DChar lhs, const DString & rhs)
{
  return DString(DString(lhs.utf8()) + rhs);
}

DString operator+(const DString & lhs, const char rhs)
{
  return DString(lhs + DChar(rhs));
}

DString operator+(const char lhs, const DString & rhs)
{
  return DString(DChar(lhs) + rhs);
}

DString operator+(const DString & lhs, const gunichar rhs)
{
  return DString(lhs + DChar(rhs));
}

DString operator+(const gunichar lhs, const DString & rhs)
{
  return DString(DChar(lhs) + rhs);
}

DString operator+(const DChar lhs, const DChar rhs)
{
  std::list<DChar> l;
  l.push_back(lhs);
  l.push_back(rhs);
  return DString(l);
}

DString operator+(const char * lhs, const DChar rhs)
{
  return DString(lhs) + rhs;
}

DString operator+(const DChar lhs, const char * rhs)
{
  return lhs + DString(rhs);
}

