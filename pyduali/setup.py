#!/usr/bin/env python
#
#---
# $Id$
#
# ------------
# Description:
# ------------
#
# This setup script will automate the installation of duali and the
# pyduali package.
# 
# In order to create executable distributions for the Windows environment.
# You will need py2exe module installed.
# 
# How to run it:
#
# C:\path\to\script> python setup.py py2exe --packages encodings
#
# Note the 'packages' argument, if you don't include it, UTF-8 and CP1256
# encoding related functionalities may not work properly.
#
#
# (C) Copyright 2003, Arabeyes, Mohammed Elzubeir
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
#  This program is written under the BSD License.
#---


from distutils.core import setup
#import py2exe

setup(name="pyduali",
      version="0.2.0",
      description="The Arabic Spellchecker",
      author="Mohammed Elzubeir",
      author_email="elzubeir@arabeyes.org",
      url="http://www.arabeyes.org/project.php?proj=duali",
      license="BSD License",
      data_files = [ ('etc', ['duali.conf']) ],
      packages = { 'pyduali': 'pyduali' },
      scripts= ['duali', 'trans2arabic', 'arabic2trans', 'dict2db'])
      

