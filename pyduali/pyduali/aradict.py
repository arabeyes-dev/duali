#---
# $Id$
#
# ------------
# Description:
# ------------
# The dict class should load the dictionary and compatibility tables into
# useful data structures. 
#
# Note that the compatibility tables are not implemented yet.
#
# (C) Copyright 2003, Arabeyes, Mohammed Elzubeir
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
#  This program is written under the BSD License.
#---

import anydbm,os
import aramisc
from arabic import *

class aradict:

  def __init__(self, data_path="../data/", verbose = 0):
    """
    Initialize the aradict class
    The default path to the data files is set to match the CVS heirarchy, this
    is unlikely to be the case once deployed
    """
    self.verbose = verbose
    dp = os.listdir(data_path)

    for myfile in dp:
      if os.path.splitext(myfile)[0] == "stemsdb":
	self.f_stems = "%s%s" % (data_path, myfile)
      if os.path.splitext(myfile)[0] == "prefixesdb":
	self.f_pref = "%s%s" % (data_path, myfile)
      if os.path.splitext(myfile)[0] == "suffixesdb":
	self.f_suf = "%s%s" % (data_path, myfile)
    
    try:
      self.stems = anydbm.open(self.f_stems)
      self.pref = anydbm.open(self.f_pref)
      self.suf = anydbm.open(self.f_suf)
    except:
      import sys
      print """
Fatal error opening dictionary db files. Please make sure that your path
includes the trailing slash (or backslash if on Windows)
"""
      sys.exit(0)

  def getPrefix(self):
    "Return the prefix hash"
    return self.pref

  def getSuffix(self):
    "Return the suffix hash"
    return self.suf

  def getStem(self):
    "Return the stem hash"
    return self.stems
  
  def loadCTable(self, filename):
    "Load the compatibility tables from a file and return in a list"
    mytable = [] 
    lines = open(filename, 'r').readlines()
    for line in lines:
      line = aramisc.chomp(line)
      mytable.append(line)
    if (self.verbose):
      print "aradict::loadCTable()"
      print "\tReturning list of size: %d" % len(mytable)
    return mytable

