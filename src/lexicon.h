/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali lexicon.h
 *
 * lexicon class definition done by Mohammed Yousif
 *
 * (C) Copyright 2002,2003,2004 Arabeyes, Mohammed Elzubeir
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#ifndef LEXICON_H
#define LEXICON_H 1


#include "general.h"

/**
 * Lexicon performs morphological analysis on a given string
 *
 * There are 2 types of analysis performed here:
 *  1. Getting the stem of a word and then its root
 *  [OR]
 *  2. Segmenting a word and looking up its prefix, stem, suffix
 *     until there is a match.
 *
 * A feature that is currently missing from this class (which would
 * dramtically improve accuracy is a compatibility table for which
 * we can check if a combination of a prefix+stem+suffix is actually
 * compatible.
 *
 * TODO:
 *  - compatibility table implementation [prefix+stem+sufix]
 * 
 */

class lexicon {

 public:
  
  /**
   * DictionaryData structure
   * 
   * NOTE: This is deprecated and will soon be removed.
   */
  struct DictionaryData {
    DictionaryData(): noLetters(0),CaseID(0), Word("") {}
    DictionaryData(int noLetters, int CaseID, DString Word):
      noLetters(noLetters),CaseID(CaseID), Word(Word) {}
    int noLetters;
    int CaseID;
    DString Word;
  };
  
  /**
   * lexicon constructor
   *
   */
  lexicon(bool = false);
  
  /**
   * Strip punctuations, digits, ascii letters and whitespace
   *
   * @param DString
   * @return DString
   */
  DString stripExtras(DString);

  /**
   * Strip diacritic marks
   * 
   * @param DString
   */
  DString stripDiacritics(DString);

  /**
   * Strip punctuations
   *
   * @param DString
   * @return DString
   */
  DString stripPunctuations(DString);

  /**
   * Segment a word to a vector of possible prefix+stem+suffix combinations
   * 
   * @param DString
   * @return std::vector<DString>
   */
  std::vector<DString> segment(DString);

  /**
   * Normalize a word by:
   *   1. Removing ALEF_MADDA ALEF_HAMZA ALEF_HAMZA_BELOW from an ALEF
   *   2. Combinging a YEH and HAMZA into a YEH_HAMZA
   *   3. Replacing an ALEF_MAKSURA with a YEH
   *   4. Replacing a TEH_MARBUTA with a HEH
   * 
   * This method serves the purpose of simplifying a word since it is common
   * to have words written with the above variations.
   *
   * @param DString
   * @return DString
   */
  DString normalize(DString);

  /**
   * Get the stem of a given word
   *
   * @param DString
   * @return DString
   */
  DString getStem(DString);

  /**
   * Get the root of a given word
   *
   * @param DString
   * @return DString
   */
  DString getRoot(DString);
  

  /**
   * Get the dictionary
   *
   * NOTE: This method is deprecated and will be removed soon.
   */
  std::map<DString, DictionaryData> getDict();

  /**
   * Get failures (words that failed to find matches in dictionary)
   *
   * NOTE: This method is deprecated and will be removed soon.
   */
  std::map<DString, int> getFailures();
  
  
 private:
  /**
   * Get the root of a 4 letter word
   *
   * @param DString
   * @return DString
   */
  DString getRoot4(DString);

  /**
   * Get the root of a 5 letter word
   *
   * @param DString
   * @return DString
   */
  DString getRoot5(DString);

  /**
   * Get the root of a 6 letter word
   * 
   * @param DString
   * @return DString
   */
  DString getRoot6(DString);

  /**
   * Dictionary (to hold data)
   *
   * NOTE: This is deprecated.
   */
  std::map<DString, DictionaryData> dict;

  /**
   * Table of non-roots
   * 
   * NOTE: This is deprecated.
   */
  std::map<DString, int> nonroots;

  /**
   * Boolean (stem or not a stem)
   */
  bool nostem;
};

#endif
