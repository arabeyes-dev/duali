/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali dchar.cc
 *
 * dchar class implementation done by Mohammed Yousif
 *
 * (C) Copyright 2002,2003, 2004 Arabeyes, Mohammed Elzubeir
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#include "general.h"

DChar::DChar()
{
    ucs4 = 0x0000;
}

DChar::DChar(const int c) throw(std::domain_error): ucs4(c)  
{
  if (!g_unichar_validate(ucs4))
    throw std::domain_error("The given value is not a valid Unicode character");
}

DChar::DChar(const gunichar c) throw(std::domain_error): ucs4(c)
{
  if (!g_unichar_validate(ucs4))
    throw std::domain_error("The given value is not a valid Unicode character");
}

DChar::DChar(const char c) : ucs4(c)
{
}

DChar::DChar(const char * c) throw(std::domain_error)
{
  *this = fromUtf8(c);
}

DChar::DChar(const std::string c) throw(std::domain_error)
{
  *this = fromUtf8(c);
}
 
DChar DChar::fromUtf8(const char * c) throw(std::domain_error)
{
  DChar dchar;
  dchar.ucs4 = g_utf8_get_char_validated(c, -1);
  if (dchar.ucs4 == static_cast<gunichar>(-1)) {
    dchar.ucs4 = 0x0000;
    throw std::domain_error("The given address does not point to a valid UTF-8 encoded Unicode character");
  } else if (dchar.ucs4 == static_cast<gunichar>(-2)) {
    dchar.ucs4 = 0x0000;
    throw std::domain_error("The given address points to a partial sequence at the end of a string that could begin a valid character");
  }
  return dchar;
}

DChar DChar::fromUtf8(const std::string c) throw(std::domain_error)
{
  return fromUtf8(c.c_str());
}


DChar DChar::lower() const
{
  DChar c;
  c.ucs4 = g_unichar_tolower(ucs4);
  return c;
}

DChar DChar::upper() const
{
  DChar c;
  c.ucs4 = g_unichar_toupper(ucs4);
  return c;
}

DChar DChar::title() const
{
  DChar c;
  c.ucs4 = g_unichar_totitle(ucs4);
  return c;
}

int DChar::digitValue() const throw(std::domain_error)
{
  int val = g_unichar_digit_value(ucs4);
  if (val == -1)
    throw std::domain_error("The character is not a decimal digit");
  else
    return val;
}

int DChar::hexDigitValue() const throw(std::domain_error)
{
  int val = g_unichar_xdigit_value(ucs4);
  if (val == -1)
    throw std::domain_error("The character is not a hexadecimal digit");
  else
    return val;
}

bool DChar::isDefined() const
{
  return g_unichar_isdefined(ucs4);
}

bool DChar::isNull() const
{
  return ucs4 == 0x0000;
}

bool DChar::isPrint() const
{
  return g_unichar_isprint(ucs4);
}

bool DChar::isGraph() const
{
  return g_unichar_isgraph(ucs4);
}

bool DChar::isPunct() const
{
  return g_unichar_ispunct(ucs4);
}

bool DChar::isSpace() const
{
  return g_unichar_isspace(ucs4);
}

bool DChar::isLower() const
{
  return g_unichar_islower(ucs4);
}

bool DChar::isUpper() const
{
  return g_unichar_isupper(ucs4);
}

bool DChar::isTitle() const
{
  return g_unichar_istitle(ucs4);
}

bool DChar::isAlphaNumeric() const
{
  return g_unichar_isalnum(ucs4);
}

bool DChar::isAsciiLetter() const
{
  return ( (ucs4 >= 0x0041 && ucs4 <= 0x005A) ||
           (ucs4 >= 0x0061 && ucs4 <= 0x007A)
         );
}
bool DChar::isDigit() const
{
  return g_unichar_isdigit(ucs4);
}

bool DChar::isHexDigit() const
{
  return g_unichar_isxdigit(ucs4);
}

bool DChar::isAlphabetic() const
{
  return g_unichar_isalpha(ucs4);
}

bool DChar::isControl() const
{
  return g_unichar_iscntrl(ucs4);
}

int DChar::bytes() const
{
  return g_unichar_to_utf8(ucs4, 0);
}

std::string DChar::utf8() const
{
  char b[7];
  int len;
  len = g_unichar_to_utf8(ucs4, b);
  b[len] = '\0';
  return std::string(b);
}

DChar::operator gunichar()
{
  return ucs4;
}

bool operator<(const DChar lhs, const DChar rhs)
{
  return lhs.ucs4 < rhs.ucs4;
}

bool operator>(const DChar lhs, const DChar rhs)
{
  return lhs.ucs4 > rhs.ucs4;
}

std::ostream & operator<<( std::ostream & out, DChar c)
{
  char b[7];
  int len;
  len = g_unichar_to_utf8(c.ucs4, b);
  b[len] = '\0';
  out << b;
  return out;
}

std::istream & operator>>( std::istream & in, DChar & c)
{
  std::string buff;
  in >> buff;
  DChar new_char(buff.c_str());
  c = new_char;
  return in;
}
