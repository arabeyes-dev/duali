/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali test
 *
 * This is just a test file to see the implementation of the lexicon working
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#include "general.h"

int main()
{
  /*
  DChar c = 0x0633;
  DChar b;
  try {
    b = "م";
  } catch (std::domain_error e) {
    std::cout << "EX: " << e.what() << std::endl;
  }
  //std::cout << "c: " << c << "b: " << b << std::endl;
  //std::cin >> b;
  //std::cout << "Char is (" << b << ")" << std::endl;
  
  try {
    std::cout << "Digit value is (" << c.digitValue() << ")" << std::endl;
  } catch (std::domain_error e) {
    std::cout << "Error: " << e.what() << std::endl;
  }
  */
  /*
  DString m = "1ض23ث67ق9";
  DString m2 = "TTTTTTT";
  DString mm = m+=DChar(0x0633);
  DString m3;
  m3 = 'R';
  try {
    std::cout << mm.substr(2, 3) << std::endl;
  } catch (std::out_of_range e) {
    std::cout << "Exception Thrown: "<< e.what() << std::endl;
  }
  std::cout << m + m2 + "محمد" << std::endl;
  std::cout << m << std::endl;
  
  m.setAt(8, DChar("أ"));
  std::cout << m << std::endl;
  
  DString mohammed = "محمد يوسف";
  try {
    std::cout << mohammed.substr(5).reverse() << std::endl;
  } catch (std::out_of_range e) {
    std::cout << "Exception Thrown: "<< e.what() << std::endl;
  }
  */

  lexicon aralex;
  std::cout << "Strip diacritics from: " << DString("مُحَمّدْ") << " -> " << aralex.stripDiacritics(DString("مُحَمّدْ")) << std::endl;

  std::cout << "normalize: " << DString("مخيءفقة غى") << " -> " << aralex.normalize(DString("مخيءفقة غى")) << std::endl;
  
  try {
    std::cout << "Root of: " << DString("راجلون") << " -> " << aralex.getRoot(DString("راجلون")) << std::endl;
    std::cout << "Stem of: " << DString("متلازمة") << " -> " << aralex.getStem(DString("متلازمة")) << std::endl;    
  } catch (DRegex::regex_error e) {
    std::cout << "Exception thrown: " << e.what() << std::endl;
  } catch (std::out_of_range e) {
    std::cout << "Exception thrown: " << e.what() << std::endl;
  }

  
  
  /*
  try {
    std::string exp = "^(\\S)([AwTdy])(\\S\\S)$";
    //exp = "^([^[:space:]])([AwTdy])([a-zA-Z][a-zA-Z])$";
    //exp = "[a-zA-Z]";
    std::string str = "rAjl";
    //str = "e";
    DRegex re(exp);
    std::cout << "Expression: " << exp << std::endl;
    std::cout << "String: " << str << std::endl;
    std::cout << "Matches: " << re.match(str) << std::endl;
    std::cout << "Match: " << re[0] << std::endl;
    std::cout << "Group 1: " << re[1] << std::endl;
    std::cout << "Group 2: " << re[2] << std::endl;
    std::cout << "Group 3: " << re[3] << std::endl;
  } catch (DRegex::regex_error e) {
    std::cout << "Exception thrown: " << e.what() << std::endl;
  } catch (std::domain_error e) {
    std::cout << "Exception thrown: " << e.what() << std::endl;
  } catch (std::out_of_range e) {
    std::cout << "Exception thrown: " << e.what() << std::endl;
  }
  */
  return 0;
}
