/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali arabic.h
 *
 * (C) Copyright 2002,2003,2004 Arabeyes, Mohammed Elzubeir
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#ifndef ARABIC_H
#define ARABIC_H 1

#include "general.h"

/** 
 *  Arabic ISO-8859-6 (subset of 10646; 0600 - 06FF)
 */

//namespace duali {

const DChar COMMA(0x60C);
const DChar SEMICOLON(0x61B);
const DChar QUESTION(0x61F);
const DChar HAMZA(0x621);
const DChar ALEF_MADDA(0x622);
const DChar ALEF_HAMZA_ABOVE(0x623);
const DChar WAW_HAMZA(0x624);
const DChar ALEF_HAMZA_BELOW(0x625);
const DChar YEH_HAMZA(0x626);
const DChar ALEF(0x627);
const DChar BEH(0x628);
const DChar TEH_MARBUTA(0x629);
const DChar TEH(0x62a);
const DChar THEH(0x62b);
const DChar JEEM(0x62c);
const DChar HAH(0x62d);
const DChar KHAH(0x62e);
const DChar DAL(0x62f);
const DChar THAL(0x630);
const DChar REH(0x631);
const DChar ZAIN(0x632);
const DChar SEEN(0x633);
const DChar SHEEN(0x634);
const DChar SAD(0x635);
const DChar DAD(0x636);
const DChar TAH(0x637);
const DChar ZAH(0x638);
const DChar AIN(0x639);
const DChar GHAIN(0x63a);
const DChar TATWEEL(0x640);
const DChar FEH(0x641);
const DChar QAF(0x642);
const DChar KAF(0x643);
const DChar LAM(0x644);
const DChar MEEM(0x645);
const DChar NOON(0x646);
const DChar HEH(0x647);
const DChar WAW(0x648);
const DChar ALEF_MAKSURA(0x649);
const DChar YEH(0x64a);

const DChar FATHATAN(0x064b);
const DChar DAMMATAN(0x064c);
const DChar KASRATAN(0x064d);
const DChar FATHA(0x64e);
const DChar DAMMA(0x64f);
const DChar KASRA(0x650);
const DChar SHADDA(0x651);
const DChar SUKUN(0x652);

const DChar MADDA_ABOVE(0x653);
const DChar HAMZA_ABOVE(0x654);
const DChar HAMZA_BELOW(0x655);
const DChar ZERO(0x660);

const DChar ONE(0x661);
const DChar TWO(0x662);
const DChar THREE(0x663);
const DChar FOUR(0x664);
const DChar FIVE(0x665);
const DChar SIX(0x666);
const DChar SEVEN(0x667);
const DChar EIGHT(0x668);
const DChar NINE(0x669);
const DChar PERCENT(0x66a);
const DChar DECIMAL(0x66b);
const DChar THOUSANDS(0x66c);
const DChar STAR(0x66d);
const DChar MINI_ALEF(0x0670);
const DChar ALEF_WASLA(0x0671);
const DChar FULL_STOP(0x06d4);
const DChar BYTE_ORDER_MARK(0xfeff);

//}

#endif  /* !ARABIC_H */
