#---
# $Id$
#
# ------------
# Description:
# ------------
# This is the transliteration table to convert the Buckwalter Morphological
# Analyzer data set from its transliterated form to proper UTF-8 encoded
# data.
#
# NOTE: I'm not sure who else is using this kind of transliteration, but I
# suspect it to be popular in the academic circles.
#
# (C) Copyright 2003, Arabeyes, Mohammed Elzubeir
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
#  This program is written under the BSD License.
#---

from arabic import *

t2a_table = { 
'A': ALEF, 
'b': BEH,
't': TEH,
'p': TEH_MARBUTA,
'v': THEH,
'j': JEEM,
'H': HAH,
'x': KHAH,
'd': DAL,
'*': THAL,
'r': REH,
'z': ZAIN,
's': SEEN,
'$': SHEEN,
'S': SAD,
'D': DAD,
'T': TAH,
'Z': ZAH,
'E': AIN,
'g': GHAIN,
'f': FEH,
'q': QAF,
'k': KAF,
'l': LAM,
'm': MEEM,
'n': NOON,
'h': HEH,
'w': WAW,
'y': YEH,
'Y': ALEF_MAKSURA,
'\'': HAMZA,
'&': WAW_HAMZA,
'>': ALEF_HAMZA_ABOVE,
'<': ALEF_HAMZA_BELOW,
'|': ALEF_MADDA,
'}': YEH_HAMZA,
'_': TATWEEL,
'a': FATHA,
'F': FATHATAN,
'i': KASRA,
'K': KASRATAN,
'u': DAMMA,
'N': DAMMATAN,
'~': SHADDA,
'o': SUKUN,
'`': MINI_ALEF,
'{': ALEF_WASLA
}

a2t_table = {
ALEF: 'A',
BEH: 'b',
TEH: 't',
TEH_MARBUTA: 'p',
THEH: 'v',
JEEM: 'j',
HAH: 'H',
KHAH: 'x',
DAL: 'd',
THAL: '*',
REH: 'r',
ZAIN: 'z',
SEEN: 's',
SHEEN: '$',
SAD: 'S',
DAD: 'D',
TAH: 'T',
ZAH: 'Z',
AIN: 'E',
GHAIN: 'g',
FEH: 'f',
QAF: 'q',
KAF: 'k',
LAM: 'l',
MEEM: 'm',
NOON: 'n',
HEH: 'h',
WAW: 'w',
YEH: 'y',
ALEF_MAKSURA: 'Y',
HAMZA: '\'',
WAW_HAMZA: '&',
ALEF_HAMZA_ABOVE: '>',
ALEF_HAMZA_BELOW: '<',
ALEF_MADDA: '|',
YEH_HAMZA: '}',
TATWEEL: '_',
FATHA: 's',
FATHATAN: 'F',
KASRA: 'i',
KASRATAN: 'K',
DAMMA: 'u',
DAMMATAN: 'N',
SHADDA: '~',
SUKUN: 'o',
MINI_ALEF: '`',
ALEF_WASLA: '{',
COMMA: ','
}
