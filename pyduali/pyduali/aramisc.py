#---
# $Id$
#
# ------------
# Description:
# ------------
#
# Miscellaneous functions (and random too!) 
#
# (C) Copyright 2003, Arabeyes, Mohammed Elzubeir
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
#  This program is written under the BSD License.
#---

from trans_table import *

def chomp(s):
  if (s.endswith('\n')):
    return s[:-1]
  else:
    return s
	
def t2utf8(s):
  "Tranliteration to UTF-8 conversion of a string"
  mystr = ''
  for mychar in s:
    mystr = "%s%s" % (mystr , trans_table.get(mychar, mychar))
    return mystr.encode('utf-8')

