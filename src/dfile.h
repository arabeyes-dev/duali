/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali dfile.h
 *
 * Dfile class definition
 *
 * (C) Copyright 2002,2003,2004 Arabeyes, Mohammed Elzubeir
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#ifndef DFILE_H
#define DFILE_H 1

#include "general.h"
using namespace std;

/**
 * File input/output and encoding verification
 * 
 * This is where we simply read files, verify they are utf-8 encoded
 * and pass their content where they can be better used.
 * 
 */
class DFile
{

 private:
  gchar *txt;
  GError *error;
  gsize fsize;
  gchar *filename;

 public:

  /**
   * default constructor
   */
  DFile () : error(NULL), txt(NULL), fsize(0) { };

  /**
   * set the file name to open
   *
   * @param const gchar *
   */
  virtual void setFilename (gchar *);

  /**
   * get the file name
   *
   * @return const gchar *
   */
  virtual gchar *getFilename () const;

  /**
   * loadFile contents to a str
   *
   * @return gboolean FALSE on failure
   */
  virtual gboolean loadFile ();
  
  /**
   * verify file for input
   * 
   * @param ifstream &, const gchar *
   */
  virtual void verify (ifstream &, const gchar *);

  /**
   * verify file for output
   *
   * @param ofstream &, const gchar *
   */
  virtual void verify (ofstream &, const gchar *);

  
  /**
   * return the entire content of a utf-8 text file
   *
   * @return gchar *
   */
  virtual gchar *getContents ();
  
};

#endif
