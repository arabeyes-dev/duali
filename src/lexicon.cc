/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali lexicon.cc
 *
 * lexicon class implementaion done by Mohammed Yousif
 *
 * This class is a port of the pyduali/aralex.py class
 *
 * (C) Copyright 2002,2003,2004 Arabeyes, Mohammed Elzubeir
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#include "general.h"


lexicon::lexicon(bool nostem): nostem(nostem)
{
}

DString lexicon::stripExtras(DString str)
{
  /*
  //std::string exp = '[' + dPunctuation + dDigits + dAsciiletters + dWhitespace + "]";
  DRegex re("[0123456789]", "g");
  return re.sub(str," ");
  */
  
  for (int i=0; i< str.length(); i++) {
    DChar c = str[i];
    if (c.isPunct() || c.isDigit() || c.isHexDigit() ||
          c.isSpace() || c.isAsciiLetter())
      str.setAt(i, ' ');
  }

  return str;
}

DString lexicon::stripDiacritics(DString str)
{
  DString exp = '[' + FATHATAN + DAMMATAN + TATWEEL + KASRATAN +
                      FATHA    + DAMMA    + KASRA   + SUKUN    +
	              SHADDA   + ']';
  
  exp = Transliterator::Transliterate(exp);
  DString str_t = Transliterator::Transliterate(str);
  
  DRegex re(exp.utf8());
  return Transliterator::DeTransliterate(re.sub(str_t,""));
}

DString lexicon::stripPunctuations(DString str)
{
  DString exp = '[' + COMMA   + SEMICOLON + QUESTION + ZERO      +
                      PERCENT + THOUSANDS + STAR     + FULL_STOP +
		']';
  
  exp = Transliterator::Transliterate(exp);
  DString str_t = Transliterator::Transliterate(str);
  
  DRegex re(exp.utf8());
  return Transliterator::DeTransliterate(re.sub(str_t,""));
}
  
/*"""
  Segment word to prefix, stem, suffix - return list of possible
  combinations
  """ */
std::vector<DString> lexicon::segment(DString str)
{
  std::vector<DString> seg_words;
  int pref_len = 0;
  int suf_len = 0;
  int str_len = str.length();
  while (pref_len <= 4) {
    DString prefix = str.substr(0, pref_len);
    int stem_len = str_len - pref_len;
    suf_len = 0;
    while (stem_len >= 1 && suf_len <= 6) {
      DString stem = str.substr(pref_len, stem_len);
      DString suffix;
      if (pref_len+stem_len < str_len)
        suffix = str.substr(pref_len+stem_len);
      else
        suffix = "";
      seg_words.push_back(prefix + stem + suffix);
      stem_len -= 1;
      suf_len += 1;
    }
    pref_len +=1;
  }
  return seg_words;
}

/*
  Normalize word
*/
DString lexicon::normalize(DString str)
{
  DString exp = '[' + ALEF_MADDA + ALEF_HAMZA_ABOVE + ALEF_HAMZA_BELOW + ']';
  exp = Transliterator::Transliterate(exp);
  DString str_t = Transliterator::Transliterate(str);
  DChar rep = Transliterator::Transliterate(ALEF);
  DRegex re(exp.utf8());
  DString str1 = re.sub(str_t.utf8(), rep.utf8());

  exp = Transliterator::Transliterate(YEH + HAMZA);
  rep = Transliterator::Transliterate(YEH_HAMZA);
  re.compile(exp.utf8());
  DString str2 = re.sub(str1.utf8(), rep.utf8());

  exp = Transliterator::Transliterate(ALEF_MAKSURA);
  rep = Transliterator::Transliterate(YEH);
  re.compile(exp.utf8());
  DString str3 = re.sub(str2.utf8(), rep.utf8());

  exp = Transliterator::Transliterate(TEH_MARBUTA);
  rep = Transliterator::Transliterate(HEH);
  re.compile(exp.utf8());
  DString str4 = re.sub(str3.utf8(), rep.utf8());
  return Transliterator::DeTransliterate(str4);
}

/* Return the root of a 4-letter word */
DString lexicon::getRoot4(DString str)
{

  DString exp = "^(\\S\\S)([" + ALEF + YEH + WAW + "])(\\S)$";
  exp = Transliterator::Transliterate(exp);
  DString str_t = Transliterator::Transliterate(str);
  DRegex re(exp.utf8());
  int m1 = re.match(str_t);
  exp = "^(\\S)([" + ALEF + WAW + TAH + DAL + YEH + "])(\\S\\S)$";
  exp = Transliterator::Transliterate(exp);

  re.compile(exp.utf8());
  int m2 = re.match(str_t);

  if (m1 > 0) {
    DString myroot = str.substr(0, 2) + str[3];
    dict[myroot] = DictionaryData(4, 1, str);
    return myroot;
  } else if (m2 > 0) {
    DString myroot = str[0] + str.substr(2);
    dict[myroot] = DictionaryData(4, 2, str);
    return myroot;
  } else {
    nonroots[str] = 4;
    return "";
  }
}

/* Return the root of a 5-letter word */
DString lexicon::getRoot5(DString str)
{
  DString str_t = Transliterator::Transliterate(str);
  
  DString exp = "^(\\S\\S)" + ALEF + ALEF + "(\\S)$";
  exp = Transliterator::Transliterate(exp);
  DRegex re(exp.utf8());
  int m1 = re.match(str_t);
  
  exp = "^(\\S)([" + TEH + YEH + "])(\\S)" + ALEF + "(\\S)$";
  exp = Transliterator::Transliterate(exp);
  re.compile(exp.utf8());
  int m2 = re.match(str_t);
  
  exp = "^(\\S)" + WAW + ALEF + "(\\S\\S)$";
  exp = Transliterator::Transliterate(exp);
  re.compile(exp.utf8());
  int m3 = re.match(str_t);
  
  exp = "^(\\S)" + ALEF + "(\\S)([" + YEH + WAW + "])(\\S)$";
  exp = Transliterator::Transliterate(exp);
  re.compile(exp.utf8());
  int m4 = re.match(str_t);
  
  exp = "^(\\S\\S\\S)([" + ALEF + YEH + WAW + "])(\\S)$";
  exp = Transliterator::Transliterate(exp);
  re.compile(exp.utf8());
  int m5 = re.match(str_t);
  
  exp = "^(\\S\\S)([" + ALEF + YEH + "])(\\S\\S)$";
  exp = Transliterator::Transliterate(exp);
  re.compile(exp.utf8());
  int m6 = re.match(str_t);

  if (m1 > 0) {
    DString myroot = str.substr(0, 2) + str.substr(4);
    dict[myroot] = DictionaryData(5, 1, str);
    return myroot;
  } else if (m2 > 0) {
    DString myroot = str[0] + str[2] + str[4];
    dict[myroot] = DictionaryData(5, 2, str);
    return myroot;
  } else if (m3 > 0) {
    DString myroot = str[0] + str.substr(3);
    dict[myroot] = DictionaryData(5, 3, str);
    return myroot;
  } else if (m4 > 0) {
    DString myroot = str[0] + str[2] + str[4];
    dict[myroot] = DictionaryData(5, 4, str);
    return myroot;
  } else if (m5 > 0) {
    DString myroot = str.substr(0, 3) + str[4];
    dict[myroot] = DictionaryData(5, 5, str);
    return myroot;
  } else if (m6 > 0) {
    DString myroot = str.substr(0, 2) + str.substr(3);
    dict[myroot] = DictionaryData(5, 6, str);
    return myroot;
  } else {
    nonroots[str] = 5;
    return "";
  }
}

/* Return the root of a 6-letter word */
DString lexicon::getRoot6(DString str)
{
  DString str_t = Transliterator::Transliterate(str);
  DString exp = "^(\\S)" + WAW + ALEF + "(\\S)" + YEH + "(\\S)$";
  exp = Transliterator::Transliterate(exp);
  DRegex re(exp.utf8());
  int m1 = re.match(str_t);

  exp = "^(\\S)(\\S)" + ALEF + "(\\S)" + YEH + "(\\S)$";
  exp = Transliterator::Transliterate(exp);
  re.compile(exp.utf8());
  int m2 = re.match(str_t);
  
  if (m1 > 0) {
    DString myroot = str[0] + str[3] + str[5];
    dict[myroot] = DictionaryData(6, 1, str);
    return myroot;
  } else if (m2 > 0) {
    DString myroot = str.substr(0, 2) + str[3] + str.substr(5);
    dict[myroot] = DictionaryData(6, 2, str);
    return myroot;
  } else {
    nonroots[str] = 6;
    return "";
  }
}

/* Return the stem, stripping prefix and suffix */
DString lexicon::getStem(DString str)
{
  DString str_t = Transliterator::Transliterate(str);
  
  DString exp = "^([" + WAW + FEH + BEH + ']' + ALEF + LAM + "|[" +
  BEH + YEH + LAM + MEEM + TEH + WAW + SEEN + NOON + ']' + TEH + "|[" +
  BEH + LAM + WAW + KAF + FEH + ']' + MEEM + "|[" + ALEF + LAM + ']' +
  LAM + "|[" + WAW + LAM + SEEN + FEH + ']' + YEH + "|[" +
  WAW + FEH + LAM + BEH + ']' + ALEF + "|)(.+?)(" + ALEF + TEH +
  '|' + WAW + ALEF + '|' + TEH + ALEF + '|' + WAW + NOON + '|' + WAW + HEH +
  '|' + ALEF + NOON + '|' + TEH + YEH + '|' + TEH + HEH + '|' + TEH + MEEM +
  '|' + KAF + MEEM + '|' + HEH + '[' + NOON + MEEM + "]|" + HEH + ALEF +
  '|' + YEH + TEH_MARBUTA + '|' + TEH + KAF + '|' + NOON + ALEF +
  '|' + YEH + '[' + NOON + HEH + "]|[" + TEH_MARBUTA + HEH + YEH + ALEF +
  "])$";
  exp = Transliterator::Transliterate(exp);
  DRegex re(exp.utf8());
  int m1 = re.match(str_t);
  
  if (m1 > 0) {
    return Transliterator::DeTransliterate(re[2]);
  } else if (m1 == 0) {
    return str;
  }
}

/* Return the root of an Arabic word */
DString lexicon::getRoot(DString str)
{
  if (nostem)
    str = stripDiacritics(str);
  else
    str = stripDiacritics(getStem(str));
  
  int len = str.length();
  if (len <=3 && len > 1) {
    dict[str] = DictionaryData(1, 0, str);
    return str;
  } else if (len == 4)
    return getRoot4(str);
  else if (len == 5)
    return getRoot5(str);
  else if (len == 6)
    return getRoot6(str);
  else {
    nonroots[str] = 0;
    return "";
  }
}
  
std::map<DString, lexicon::DictionaryData> lexicon::getDict()
{
  return dict;
}

std::map<DString, int> lexicon::getFailures()
{
  return nonroots;
}
