/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Duali general.h
 *
 * General header file and definitions
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 *  Duali is licensed under the BSD license. See LICENSE file.
 *
 ************************************************************************/

#ifndef GENERAL_H
#define GENERAL_H 1


#include <iostream>
#include <sstream>
#include <vector>
#include <map>
#include <list>
#include <exception>
#include <stdexcept>
#include <fstream>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <locale.h>
#include <sys/types.h>
#include <getopt.h>
#include <glib.h>
#include <regex.h>
#include <db.h>

#include "dfile.h"
#include "error.h"
#include "dchar.h"
#include "dstring.h"
#include "lexicon.h"
#include "arabic.h"
#include "transliterator.h"
#include "dspell.h"
#include "dregex.h"

typedef gunichar unichar;

#ifdef __cplusplus
#define BEGIN_C_DECLS extern "C" {

#define END_C_DECLS   }
#else // !__cplusplus
#define BEGIN_C_DECLS


#define END_C_DECLS

#endif // __cplusplus


#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1
#endif

extern char *program_name;
char *program_version ();

#endif // !GENERAL_H
